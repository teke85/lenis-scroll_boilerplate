import Image from 'next/image';
import Lenis from './components/Lenis';

export default function Home() {
  return (
    <Lenis>
      <section className="h-screen">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro
        blanditiis voluptas voluptate aspernatur distinctio cumque fuga qui
        exercitationem vero animi enim expedita molestias, est hic facilis et
        corporis? Culpa, ipsam a adipisci, ipsum amet doloribus iste earum id
        dignissimos voluptatem accusantium ab sapiente natus reprehenderit
        voluptatum. Repellat est ea laboriosam provident ad. In asperiores iste
        veritatis soluta suscipit, perferendis consectetur facilis error tempora
        exercitationem dicta praesentium facere dolorem est nihil quam expedita
        voluptatum, quod earum quasi. Quasi eveniet rerum voluptates amet
        deleniti illo sapiente id corporis eius qui eos voluptate pariatur
        similique debitis ea optio nostrum, sed cupiditate repudiandae eaque.
      </section>
      <section className="h-screen">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum, illum.
        Eveniet debitis maxime quidem ea impedit omnis molestiae iste facilis
        assumenda, similique veritatis, non vel harum aperiam! Aliquid
        distinctio rerum iusto impedit qui repudiandae labore deserunt cumque
        minima eius, nihil quaerat itaque recusandae ut autem illum minus
        aperiam ipsam placeat laboriosam maxime explicabo, id, et dolorem! Dicta
        tempora nihil hic quis ex ipsum delectus incidunt aspernatur impedit
        qui. Itaque molestiae tenetur magni exercitationem at. Voluptates,
        voluptate enim! Ipsa fugiat eaque expedita eos excepturi ipsum
        aspernatur quasi suscipit accusamus corrupti, alias voluptates doloribus
        sint impedit. Deleniti placeat perspiciatis laboriosam beatae
        reiciendis.
      </section>
      <section className="h-screen">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nesciunt ab
        illum velit maxime quidem nulla, natus nihil incidunt quasi dolore ullam
        debitis in. Necessitatibus vitae eligendi perferendis maxime totam
        minus. Nemo nobis, asperiores perspiciatis veritatis dignissimos maxime
        odit distinctio adipisci placeat tempore obcaecati pariatur et dolorem
        laborum repudiandae dicta in minima perferendis. Necessitatibus
        voluptatum, quasi pariatur quam ut animi quos earum beatae voluptatem
        veniam libero asperiores aliquam cum. Soluta incidunt facere dolor
        officia illo ex voluptatibus corporis mollitia deserunt ullam iure aut
        voluptatem est exercitationem ipsa, repellendus libero quod illum
        sapiente et saepe. A id sit illum sequi, facilis ipsum.
      </section>
      <section className="h-screen">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque maiores
        quaerat illo vitae id obcaecati, optio iure officiis. Dolore dicta,
        ducimus molestias aperiam soluta impedit error nam maiores voluptates
        quaerat. Perferendis ducimus provident voluptate quia! Non temporibus
        inventore adipisci. Ea, magnam doloremque reiciendis minima, distinctio
        corrupti dicta dolor repellendus amet nam laudantium eligendi optio
        recusandae commodi dolorem atque saepe cupiditate fugit voluptate
        tempora laborum exercitationem sed deserunt! Possimus minima provident
        harum numquam, nihil exercitationem eum ea iste voluptatibus, eius
        aperiam fugit unde magni consectetur id dolorem iure similique facere.
        Est quos commodi nemo illo expedita illum saepe architecto dolore sequi!
      </section>
      <section className="h-screen">
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consectetur
        iusto unde tenetur! Tempora optio saepe facere accusantium soluta labore
        incidunt tempore molestiae doloribus architecto unde illum aliquid eum
        repudiandae iusto est repellat, fugit eligendi at ad fugiat! Adipisci
        recusandae architecto, mollitia corrupti quasi officia debitis
        voluptatum deleniti iste, deserunt nam aut quos reiciendis rerum
        obcaecati, odit aperiam accusamus nesciunt laudantium dolores nobis eius
        est. Minus et incidunt laudantium dolorum voluptate culpa tempora
        repellendus illo at sequi! Numquam natus culpa minus perspiciatis nobis
        debitis provident dolore. Totam ex iusto nihil enim facere, laboriosam
        minima tempora reiciendis maxime tempore odit, dolorem sapiente.
        Aspernatur minus optio, laborum repellat animi corporis quas earum sed!
        Sint sequi cum debitis voluptates fugit vitae molestias, perferendis
        atque nemo laboriosam illum deleniti, omnis quae quo sed a! Facere quam
        esse quaerat autem eos eveniet iusto, quo qui distinctio alias
        doloribus, accusamus, corrupti id reiciendis perferendis natus corporis
        quis commodi ab vitae porro earum! Iste optio deserunt quis eos vel
        nesciunt quidem possimus molestiae excepturi commodi aliquid non dolores
        magni error quia, cupiditate adipisci voluptas voluptates laboriosam
        neque. Quia, libero voluptate unde temporibus cupiditate fugiat aut
        possimus esse eveniet, nesciunt deserunt consequuntur, culpa odit.
        Repudiandae voluptates totam laboriosam aspernatur temporibus accusamus
        pariatur, tempore inventore? Ratione praesentium optio aspernatur
        architecto nobis. Tenetur praesentium nihil explicabo at necessitatibus
        molestiae quae rem totam suscipit voluptatem quam, cum fugit sed nobis
        assumenda eos nam dignissimos ipsa, facilis enim, sunt odio quasi. Quam
        dolorem, perspiciatis alias id minus laboriosam reiciendis iusto
        ducimus, est animi repellat provident doloribus nesciunt iure odit
        repellendus. Velit voluptatem nesciunt repellat quos repellendus beatae
        quod reiciendis perspiciatis sunt eaque. Pariatur eius omnis minima
        doloribus, aut voluptatibus ut hic voluptatem suscipit alias maxime!
        Obcaecati, modi temporibus earum exercitationem, voluptate ipsam quae
        asperiores, ut facere maxime saepe odio ipsum a natus quasi excepturi
        laudantium necessitatibus? Necessitatibus laborum ad pariatur tempora id
        error quo inventore maxime dolor nisi, ducimus odit quam voluptate,
        perspiciatis, sapiente aspernatur rem culpa. Porro provident maiores
        sint aliquam dolorum doloribus fuga culpa non asperiores, excepturi quas
        nulla illo atque vero repudiandae? Sapiente dignissimos esse numquam
        nihil ratione vel tempora totam beatae. Dolor eius repellat, eos sit
        quasi esse magnam deserunt nostrum inventore est exercitationem ipsum,
        sapiente maiores excepturi odit ullam nesciunt explicabo earum eligendi
        rem nobis unde aperiam provident a. Natus eaque ullam debitis repellat
        totam dolorem repellendus voluptate magnam maxime autem dignissimos
        aliquam, eligendi odit quo distinctio. Magni saepe ea ducimus soluta
        corrupti accusantium quod dicta reprehenderit et quas, voluptas
        voluptates assumenda cum est recusandae adipisci modi eaque minus?
        Architecto assumenda exercitationem neque fugit sit libero natus
        consectetur est laborum dolor, eveniet blanditiis laudantium animi
        cupiditate ullam! Expedita, veritatis. Ducimus dolorum natus velit
        doloremque at! Totam pariatur id dolor eveniet repellendus eius fuga,
        placeat quia, optio consequatur ullam modi nisi! Quod explicabo, quas
        aperiam dignissimos deserunt dicta dolore excepturi recusandae sit
        ducimus consectetur expedita quibusdam, temporibus natus totam similique
        odit nihil numquam consequatur asperiores cupiditate vero inventore
        repellendus! Illum labore odit facere laboriosam officia repudiandae,
        soluta excepturi atque.
      </section>
    </Lenis>
  );
}
