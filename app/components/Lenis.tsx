'use client';

import React, { PropsWithChildren } from 'react';
import { ReactLenis, useLenis } from '@studio-freight/react-lenis'

const Lenis = ({ children }: PropsWithChildren) => {
  return (
    <ReactLenis root options={{ lerp: 0.1, duration: 7 }}>
      {children}
    </ReactLenis>
  );
};


export default Lenis;
